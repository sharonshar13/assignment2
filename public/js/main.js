google.charts.load('current', { 'packages': ['corechart', 'bar'] });
google.charts.load('current', { 'packages': ['bar'] });
google.charts.load('current', { 'packages': ['table'] });
google.charts.setOnLoadCallback(drawpieChart);
google.charts.setOnLoadCallback(drawbarChart);
google.charts.setOnLoadCallback(drawpieChart1);
google.charts.setOnLoadCallback(drawbarChart1);
google.charts.setOnLoadCallback(drawbarChart2);
google.charts.setOnLoadCallback(drawbarChart3);
google.charts.setOnLoadCallback(drawTable);

function drawpieChart() {
  var dataArray = [['user type', 'revision number']];
  overallPie.forEach(function (element) {
    if (element.type == "no") {
      return;
    }
    var data = [element.type, element.number];
    dataArray.push(data);
  });

  var data = google.visualization.arrayToDataTable(dataArray);
  var options = {
    title: 'Revision distribution by year and by user type',
    'width': 870,
    'height': 300
  };

  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}


function drawbarChart() {
  var rawData = {};
  overallBar.forEach(function (element) {
    if (!rawData[element.year]) {
      rawData[element.year] = {};
    }
    rawData[element.year][element.type] = element.number;
  })

  var chartData = [['Year', 'admin', 'anon', 'bot', 'regular']];
  for (var row in rawData) {
    var chartRow = [row];
    chartRow.push(rawData[row].admin);
    chartRow.push(rawData[row].anon);
    chartRow.push(rawData[row].bot);
    chartRow.push(rawData[row].regular);
    chartData.push(chartRow);
  }

  var data = google.visualization.arrayToDataTable(chartData);
  var options = {
    'width': 870,
    'height': 300,
    'title': 'Revision distribution by year and user type',
    hAxis: {
      title: 'timestamp'
    }
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('barchart'));
  chart.draw(data, options);
}


function drawbarChart1(json) {

  var rawData = {};
  json.forEach(function (element) {
    if (!rawData[element.year]) {
      rawData[element.year] = {};
    }
    rawData[element.year][element.type] = element.number;
  })

  var chartData = [['Year', 'admin', 'anon', 'bot', 'regular']];
  for (var row in rawData) {
    var chartRow = [row];
    chartRow.push(rawData[row].admin);
    chartRow.push(rawData[row].anon);
    chartRow.push(rawData[row].bot);
    chartRow.push(rawData[row].regular);
    chartData.push(chartRow);
  }
  var data = google.visualization.arrayToDataTable(chartData);

  var options = {
    'width': 870,
    'height': 300,
    'title': 'Revision distribution by year and by user type for this article',
    hAxis: {
      title: 'timestamp'
    }
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('barchart1'));
  chart.draw(data, options);
}


function drawpieChart1(json) {
  var dataArray = [['user type', 'revision number']];
  console.log(json);
  json.forEach(function (element) {
    if (element.type == "no") {
      return;
    }
    var data = [element.type, element.number];
    dataArray.push(data);
  });

  var data = google.visualization.arrayToDataTable(dataArray);
  var options = {
    title: 'Revision number distribution based on user type for article',
    'width': 870,
    'height': 300
  };
  var chart = new google.visualization.PieChart(document.getElementById('piecharts'));
  chart.draw(data, options);
}


function drawbarChart2(json) {
  var dataArray = [['Year', 'Number of revisions']];
  json.forEach(function (element) {
    var data = [element._id, element.number];
    dataArray.push(data);
  });
  var data = google.visualization.arrayToDataTable(dataArray);

  var options = {
    'width': 870,
    'height': 300,
    'title': 'Revision distribution by year of one user for this article',
    hAxis: {
      title: 'timestamp'
    }
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('barchart2'));
  chart.draw(data, options);
}

function drawTable(json) {
  var data = new google.visualization.DataTable();
  data.addColumn('string', 'Name');
  data.addColumn('number', 'Number of Revision');
  var dataArray = [];
  
  json.forEach(function (element) {
    var data = [element._id, element.numOfRevisions];
    dataArray.push(data);
  });

  data.addRows(dataArray);
  var table = new google.visualization.Table(document.getElementById('table_div'));
  table.draw(data, { showRowNumber: true, width: '50%', height: '100%' });
}

function drawbarChart3(json) {
  var dataArray = [['Year', 'Number of revisions']];
  json.forEach(function (element) {
    var data = [element._id, element.number];
    dataArray.push(data);
  });
  var data = google.visualization.arrayToDataTable(dataArray);

  var options = {
    'width': 870,
    'height': 300,
    'title': 'Revision distribution by year of top 5 users in total for this article',
    hAxis: {
      title: 'timestamp'
    }
  };
  var chart = new google.visualization.ColumnChart(document.getElementById('barchart3'));
  chart.draw(data, options);
}


$(document).ready(function () {
  $("#submit").click(function (event) {
    var name = $("#select1 :selected").val()
    var filename = encodeURIComponent(name)
    $.ajax({
      type: "GET",
      url: "/revision/revisions?title=" + filename,
      dataType: "json",
      success: function (data) {
        $("#indMessage").html(data.message)
        $("#indTitle").html(data.title)
        $("#totlRevision").html(data.numRevs)
        drawpieChart1(data.individualPie)
        drawbarChart1(data.individualBar)
        drawbarChart2(data.individualBar2)
        drawbarChart3(data.individualBar3)
        drawTable(data.top5Users)
      }
    })

    event.preventDefault();
  })
});




